core = 6.x
api = 2

projects[] = feeds_xpathparser

projects[views_datasource][version] = "1.x-dev"
projects[views_datasource][patch][] = "http://drupal.org/files/issues/missing_template_variables_1265190_2.patch"

projects[feeds][version] = "1.x-dev"
projects[feeds][patch][] = "http://drupal.org/files/issues/add_new_hook-1288814-1.patch"


projects[views_baseurl][download][type] = "git"
projects[views_baseurl][download][url] = "git://drupalcode.org/sandbox/ergonlogic/1274240.git"
projects[views_baseurl][type] = "module"

; need -dev version of UUID for Views integration
projects[uuid][version] = 1.x-dev
